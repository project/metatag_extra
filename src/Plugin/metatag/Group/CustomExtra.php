<?php
/**
 * @file
 * Contains \Drupal\metatag_extra\Plugin\metatag\Group\CustomExtra.
 */

namespace Drupal\metatag_extra\Plugin\metatag\Group;

use Drupal\metatag\Plugin\metatag\Group\GroupBase;

/**
 * Provides a plugin for the 'Custom Extra' meta tag group.
 *
 * @MetatagGroup(
 *   id = "custom_extra",
 *   label = @Translation("Custom Extra"),
 *   description = @Translation("These custom tags can be added by site administrators."),
 *   weight = 100,
 * )
 */
class CustomExtra extends GroupBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
