<?php

/**
 * This base plugin allows "base" tags to be further customized.
 */

namespace Drupal\metatag_extra\Plugin\metatag\Tag;

use \Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * Provides a plugin for the 'base' meta tag.
 *
 * @MetatagTag(
 *   id = "base",
 *   label = @Translation("Base href tag"),
 *   description = @Translation("Set the base href for the document."),
 *   name = "base",
 *   group = "advanced",
 *   weight = -1000,
 *   type = "uri",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class BaseTag extends MetaNameBase {
  /**
   * Display the meta tag.
   */
  public function output() {
    if (empty($this->value)) {
      // If there is no value, we don't want a tag output.
      $element = '';
    }
    else {
      $element = [
        '#tag' => 'base',
        '#attributes' => [
          'target' => '_self',
          'href' => $this->value(),
        ]
      ];
    }

    return $element;
  }
}
