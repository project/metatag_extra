<?php
/**
 * @file
 * Contains \Drupal\metatag_extra\Plugin\metatag\Tag\CustomTag.
 */

namespace Drupal\metatag_extra\Plugin\metatag\Tag;

use \Drupal\metatag\Plugin\metatag\Tag\MetaNameBase;

/**
 * Provides a plugin for the 'Custom Tag' meta tag.
 *
 * @MetatagTag(
 *   id = "custom_tag",
 *   label = @Translation("Custom Tag"),
 *   description = @Translation("This is an example custom tag."),
 *   name = "customTag",
 *   group = "custom_extra",
 *   weight = 0,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   deriver = "Drupal\metatag_extra\Plugin\Derivative\ExtraTags"
 * )
 */
class CustomTag extends MetaNameBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
