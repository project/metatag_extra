<?php
/**
 * @file
 * Contains \Drupal\metatag_extra\Plugin\metatag\Tag\CustomLink.
 */

namespace Drupal\metatag_extra\Plugin\metatag\Tag;

use \Drupal\metatag\Plugin\metatag\Tag\LinkRelBase;

/**
 * Provides a plugin for the 'canonical' meta tag.
 *
 * @MetatagTag(
 *   id = "custom_link",
 *   label = @Translation("Custom Link"),
 *   description = @Translation("This is an example custom link."),
 *   name = "customLink",
 *   group = "custom_extra",
 *   weight = -10,
 *   type = "uri",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   deriver = "Drupal\metatag_extra\Plugin\Derivative\ExtraLinks"
 * )
 */
class CustomLink extends LinkRelBase {
  // Nothing here yet. Just a placeholder class for a plugin.
}
