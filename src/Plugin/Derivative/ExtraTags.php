<?php

namespace Drupal\metatag_extra\Plugin\Derivative;

use Drupal\metatag_extra\ExtraTagDeriverBase;

/**
 * Provides metatag tag plugin definitions.
 *
 * @see \Drupal\metatag_extra\ExtraTagDeriverBase
 */
class ExtraTags extends ExtraTagDeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $tags = $this->getExtraTags('custom_tag');
    foreach ($tags as $tag) {
      $key = $tag['name'];
      $this->derivatives[$key] = $base_plugin_definition;
      $this->derivatives[$key]['label'] = t('Custom tag: ' . $tag['label']);
      $this->derivatives[$key]['name'] = $key;
      $this->derivatives[$key]['group'] = $tag['scope'];
    }
    return $this->derivatives;
  }
}