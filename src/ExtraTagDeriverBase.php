<?php

namespace Drupal\metatag_extra;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\Config;

/**
 * The base for all derivers that need to implement metatag plugins.
 */
abstract class ExtraTagDeriverBase extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Configuration settings for the extra tags.
   *
   * @var Config
   */
  protected $config;

  /**
   * Allowed custom plugins.
   *
   * @var string
   */
  protected $allowedPlugins = ['custom_tag', 'custom_link'];

  /**
   * Constructs new NodeBlock.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      \Drupal::config('metatag_extra.settings')
    );
  }

  /**
   * Getter function to retrieve all extra tags
   */
  public function getExtraTags($type = NULL) {
    // Return if there are no tags or if the type is not allowed
    if (!$this->config->get('tags') ||
      ($type && !in_array($type, $this->getAllowedPlugins()))) {
      return;
    }
    $tags = $this->config->get('tags');

    $tag_groups = [];
    foreach ($tags as $tag) {
      if ($tag['type']) {
        $tag_groups[$tag['type']][] = $tag;
      }
    }

    return $type ? $tag_groups[$type] : $tags;
  }

  /**
   * Getter function to retrieve an array of allowed plugins
   */
  public function getAllowedPlugins() {
    return $this->allowedPlugins ? $this->allowedPlugins : NULL;
  }
}